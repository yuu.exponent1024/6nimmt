import random

MAX_ROWS = 6
MAX_COLS = 4
PLAYER_NUM = 2
HANDS_NUM = 10
THRESHOLD = 10

class Card:
    val = 0
    point = 0
    def __init__(self,val):
        self.val = val
        if val == 55:
            self.point = 7
        elif val % 11 == 0:
            self.point = 5
        elif val % 10 == 0:
            self.point = 3
        elif val % 5 == 0:
            self.point = 2
        else:
            self.point = 1

class Field:
    field = []
    tails = []
    #コンストラクタ-場の初期化
    def __init__(self,cards):
        for i in range(MAX_COLS):
            val = cards.pop(0)
            card = Card(val)
            self.field.append([card]) #場に存在する全てのカードの[2次元配列]
            self.tails.append(card)   #連結される全てのカード[1次元]
    #場の表示を行う。   
    def disp(self):
        for i in range(MAX_COLS):
            for j in range(MAX_ROWS):
                if j > len(self.field[i])-1:
                    if j == 5:
                        print("🔴     ", end = "")
                    else:  
                        print("⚪     ", end = "")
                else:
                    print('{:<3}[{}] '.format(self.field[i][j].val,self.field[i][j].point),end="")
            print()

class Player:
    hand = []
    dmg = 0
    #コンストラクタ-プレイヤーの初期化
    def __init__(self,cards):
        hand = []
        for i in range(HANDS_NUM):
            val = cards.pop(0)
            card = Card(val)
            hand.append(card)
        self.hand = hand
        self.dmg = 0

    def disp_hand(self):
        for i in range(len(self.hand)):
            print('{:>3}[{}]'.format(self.hand[i].val,self.hand[i].point),end=",")
        print()
    
    def pop_card(self,val):
        for i in range(len(self.hand)):
            if self.hand[i].val == val:
                self.hand.pop(i)
                return

    def include_card(self,val):
        for i in range(len(self.hand)):
            if self.hand[i].val == val:
                return True
        return False

class Human(Player):
    hand = []
    dmg = 0
    name = ""
    #コンストラクタ-プレイヤーの初期化
    def __init__(self,cards):
        self.name = "YOU"
        super(Human, self).__init__(cards)
    def disp_hand(self):
        print("{}の手札: ".format(self.name.rjust(4)),end="")
        super(Human,self).disp_hand()
    def pop_card(self,val):
        super(Human,self).pop_card(val)
    def include_card(self,val):
        return super(Human,self).include_card(val)
    def select_card(self,f=None):
        print("どのカードを手札から場に出すか選択して下さい。[数字を入力]")
        inp = input()
        while not inp.isdecimal() or not self.include_card(int(inp)):
            print("指定されたカードは存在しません。入力し直して下さい。[数字を入力]")
            inp = input()

        return int(inp)  
    #カードを場に出す。
    def place_card(self,val,f):
        self.pop_card(val)
        m = -104
        idx = -1
        for i in range(MAX_COLS):
            n = f.tails[i].val - val
            if n < 0 and n > m:
                m = n
                idx = i
        #Tailsのどのカードよりも小さい場合は１つのスタックを取る        
        if idx == -1:
            print("列を取らなければなりません。取りたい列を1つ選択して下さい。[1-4]")
        while idx == -1:
            i = int(input())
            if i >= 1 and i <= 4:
                idx = i-1
                #HP処理
                for c in f.field[idx]:
                    self.dmg += c.point
                f.field[idx].clear()

                card = Card(val)
                f.tails[idx] = card
                f.field[idx].append(card)
                return
            else:
                print("番号が正しくありません。取りたい列を1つ選択して下さい。[1-4]")
        #列を取らなければいけない場合(スタックが5枚)
        if len(f.field[idx]) == MAX_ROWS-1:
            #HP処理
            for c in f.field[idx]:
                self.dmg += c.point
            f.field[idx].clear()

            card = Card(val)
            f.tails[idx] = card
            f.field[idx].append(card)
            return
        #スタックの最後尾にカードを加える
        card = Card(val)
        f.tails[idx] = card
        f.field[idx].append(card)

class Random(Player):
    hand = []
    dmg = 0
    name = ""
    #コンストラクタ-プレイヤーの初期化
    def __init__(self,cards):
        self.name = "CPU1"
        super(Random, self).__init__(cards)
    def disp_hand(self):
        print("{}の手札: ".format(self.name.rjust(4)),end="")
        super(Random,self).disp_hand()
    #手札から捨てる
    def pop_card(self,val):
        super(Random,self).pop_card(val)
    #手札にカードを含むか
    def include_card(self,val):
        return super(Random,self).include_card(val)
    #場に出すカードを選択する
    def select_card(self,f=None):
        rnd = random.randrange(len(self.hand))
        return self.hand[rnd].val
    #カードを場に出す。
    def place_card(self,val,f):
        self.pop_card(val)
        m = -104
        idx = -1
        for i in range(MAX_COLS):
            n = f.tails[i].val - val
            if n < 0 and n > m:
                m = n
                idx = i
        #Tailsのどのカードよりも小さい場合は１つのスタックを取る
        if idx == -1:
            i = random.randrange(1,5)
            if i >= 1 and i <= 4:
                idx = i-1
                #HP処理
                for c in f.field[idx]:
                    self.dmg += c.point
                f.field[idx].clear()

                card = Card(val)
                f.tails[idx] = card
                f.field[idx].append(card)
                return
        
        if len(f.field[idx]) == MAX_ROWS-1:
            #HP処理
            for c in f.field[idx]:
                self.dmg += c.point
            f.field[idx].clear()

            card = Card(val)
            f.tails[idx] = card
            f.field[idx].append(card)
            return

        card = Card(val)
        f.tails[idx] = card
        f.field[idx].append(card)

class Strategy(Player):
    hand = []
    dmg = 0
    name = ""
    #コンストラクタ-プレイヤーの初期化
    def __init__(self,cards):
        self.name = "CPU2"
        super(Strategy, self).__init__(cards)
    def disp_hand(self):
        print("{}の手札: ".format(self.name.rjust(4)),end="")
        super(Strategy,self).disp_hand()
    #手札から捨てる
    def pop_card(self,val):
        super(Strategy,self).pop_card(val)
    #手札にカードを含むか
    def include_card(self,val):
        return super(Strategy,self).include_card(val)
    #場に出すカードを選択する
    def select_card(self,f):
        diff = []
        min_h = 105
        for i in range(len(f.tails)):
            m = 103 #考えられる差の最大値 104 - 1
            for h in self.hand:
                d = h.val - f.tails[i].val
                if d > 0 and d < m:
                    m = d
                if h.val < min_h:
                    min_h = h.val
            diff.append(m)
        sorted_diff = sorted(diff)
        for s in sorted_diff:
            idx = diff.index(s)
            if len(f.field[idx]) < 5 and s < THRESHOLD:
                return f.tails[idx].val+s
        #もし上記の方策で出せるカードが無いときは....
        #ダメージが最小となるスタックを取得する
        return min_h
    #カードを場に出す。
    def place_card(self,val,f):
        #ここ加筆
        self.pop_card(val)
        m = -104
        idx = -1
        for i in range(MAX_COLS):
            n = f.tails[i].val - val
            if n < 0 and n > m:
                m = n
                idx = i
        #Tailsのどのカードよりも小さい場合は１つのスタックを取る
        min_dmg = 100
        i = -1
        for j in range(len(f.field)):
            dmg = 0
            for c in f.field[j]:
                dmg += c.point
            if dmg < min_dmg:
                min_dmg = dmg 
                i = j
        #print("ダメージ最小値:{},{}列目".format(min_dmg,i))   
        if idx == -1:     
            if i >= 0 and i < 4:
                idx = i
                #HP処理
                for c in f.field[idx]:
                    self.dmg += c.point
                f.field[idx].clear()

                card = Card(val)
                f.tails[idx] = card
                f.field[idx].append(card)
                return
        
        if len(f.field[idx]) == MAX_ROWS-1:
            #HP処理
            for c in f.field[idx]:
                self.dmg += c.point
            f.field[idx].clear()

            card = Card(val)
            f.tails[idx] = card
            f.field[idx].append(card)
            return

        card = Card(val)
        f.tails[idx] = card
        f.field[idx].append(card)

def main():
    p1_wins = 0
    p2_wins = 0
    for n in range(1000):
        #1 ~ 104 までのカードを生成しランダムに並び替える。
        cards = [i for i in range(1,105)]
        random.shuffle(cards)
        f = Field(cards)
        #f.disp()
        p1 = Strategy(cards)
        p2 = Random(cards)
        for i in range(1,HANDS_NUM+1):
            #print("【{}ターン目】 {}: {}点 , {}: {}点".format(i,p1.name,p1.dmg,p2.name,p2.dmg))
            #p1.disp_hand()
            #p2.disp_hand()
            p1_val = p1.select_card(f) 
            p2_val = p2.select_card()
            #print("CPU2:{} , CPU1:{}".format(p1_val,p2_val))

            if p1_val < p2_val:
                p1.place_card(p1_val,f)
                p2.place_card(p2_val,f)
            else:
                p2.place_card(p2_val,f)
                p1.place_card(p1_val,f)

            #f.disp()
            
        if p1.dmg < p2.dmg:
            #print("{} WIN!  👑 ".format(p1.name),end="")
            p1_wins += 1
        else:
            #print("{} LOSE! 💀 ".format(p2.name),end="")
            p2_wins += 1
        #print("【{}: {} vs {}: {}】".format(p1.name,p1.dmg,p2.name,p2.dmg))
    print(p1_wins,p2_wins)

if __name__ == "__main__":
    main()
